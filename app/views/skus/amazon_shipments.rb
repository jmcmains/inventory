#FBA Process:
#1.Create Shipping Plan
fis=Sku.fulfillment_inbound_shipment_client("Amazon US")
ship_from_address={"PostalCode"=>"27217", "Name"=>"OE Enterprises", "CountryCode"=>"US", "StateOrProvinceCode"=>"NC", "AddressLine1"=>"717 N Park Ave", "City"=>"Burlington"}
origin="Amazon US"
data=Sku.amazon(origin)
inbound_shipment_plan_request_items=[]
data.sort_by { |a| a[0] }.each do |kit|
  sku=Sku.where(name: kit[0]).first
  need_28=sku.amazon_need(kit[1],origin,28).ceil
  if need_28>0
    inbound_shipment_plan_request_items << {"SellerSKU"=>kit[0],"Quantity"=> need_28}
  end
end




isp=fis.create_inbound_shipment_plan(ship_from_address, inbound_shipment_plan_request_items, label_prep_preference: "SELLER_LABEL")
#1a.Create Product Labels
#2.Create Shipments
shipments=isp.parse["InboundShipmentPlans"]["member"]
inbound_shipment=[]
shipments.each_with_index do |shipment|
  shipment_id=shipment["ShipmentId"]
  shipment_city=shipment["ShipToAddress"]["City"]
  destination_fulfillment_center_id=shipment["DestinationFulfillmentCenterId"]
  inbound_shipment_header= {
    "ShipmentName" => "#{Date.today} - #{shipment_city}",
    "ShipFromAddress" => ship_from_address,
    "DestinationFulfillmentCenterId" => destination_fulfillment_center_id,
    "LabelPrepPreference" => "SELLER_LABEL",
    "ShipmentStatus" => "WORKING"
  }
  inbound_shipment_items=shipment["Items"]["member"]
  inbound_shipment[i]=fis.create_inbound_shipment(shipment_id, inbound_shipment_header, inbound_shipment_items: inbound_shipment_items)
end
#3.Create Product Labels

lis=fis.list_inbound_shipments(shipment_id_list: ["FBA20QNLBY"]).parse