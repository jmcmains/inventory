class ChannelAdvisor1
	include HTTParty
	
	headers 'Accept' => 'application/json', 'APICredentials' => {	'DeveloperKey' => Key.where(name: "developer_key", resource: "channeladvisor").first.value,	'Password' => Key.where(name: "password", resource: "channeladvisor").first.value}
	
	def initialize()
		@options = {
			query: {
				"accountID" =>Key.where(name: "account_id", resource: "channeladvisor").first.value
				}
		}
	end

	def get_order_list
		self.class.get("http://api.channeladvisor.com/webservices/GetOrderList", @options)
	end
end
