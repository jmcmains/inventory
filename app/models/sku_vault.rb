class SkuVault
	include HTTParty
	
	headers 'Accept' => 'application/json'
	
	def initialize()
	@options = {
		query: {
			"pageNumber" => 0,
			"tenantToken" => Key.where(name:"tenant_token",resource:"sku_vault").first.value, 
			"userToken" => Key.where(name:"user_token",resource:"sku_vault").first.value,
			"WarehouseId" => Key.where(name:"warehouse_id",resource:"sku_vault").first.value
		}
	}
	end

	def get_item_quantities
		self.class.post("https://app.skuvault.com/api/inventory/getItemQuantities", @options).parsed_response["Items"]
	end

	def get_kit_quantities
		self.class.post("https://app.skuvault.com/api/inventory/getKitQuantities", @options).parsed_response["Kits"]
	end
	
	def get_warehouse_item_quantities
		self.class.post("https://app.skuvault.com/api/inventory/GetWarehouseItemQuantities", @options).parsed_response["ItemQuantities"]
	end
	
	def get_warehouse_item_quantity(sku)
		@options[:query]["Sku"]= sku
		self.class.post("https://app.skuvault.com/api/inventory/GetWarehouseItemQuantity", @options).parsed_response["TotalQuantityOnHand"]
	end


end
