class ShipmentPlan < ActiveRecord::Base
  belongs_to :plan, class_name: "Event"
  belongs_to :shipment, class_name: "Event"
end
