class EventsController < ApplicationController
	#shipping plan
	
	def new_shipping_plan
	  origin=params[:origin]
	  @title="New Shipping Plan"
  	@show_offer = true
  	@event = Event.new(event_type: "Shipping Plan - #{origin}")
   	data=Sku.amazon(origin)
    data.sort_by { |a| a[0] }.each do |kit|
      sku=Sku.where(name: kit[0]).first
      need_28=sku.amazon_need(kit[1],origin,28).ceil
      if need_28>0
        @event.product_counts.build(sku_id: sku.id,count: need_28)
      end
    end
	end
	
	def create_shipping_plan
	  @event = Event.new(event_params)
	  @event.save!
    origin=@event.event_type[("Shipping Plan - ".length)..@event.event_type.length]
    fis=Sku.fulfillment_inbound_shipment_client(origin)
    ship_from_address={
      "PostalCode"=>"27217", 
      "Name"=>"OE Enterprises", 
      "CountryCode"=>"US", 
      "StateOrProvinceCode"=>"NC", 
      "AddressLine1"=>"717 N Park Ave", 
      "City"=>"Burlington"}
    inbound_shipment_plan_request_items=[]
    @event.product_counts.sort_by { |a| a.sku.name }.each do |pc|
      if pc.count>0
        inbound_shipment_plan_request_items << {"SellerSKU"=>pc.sku.name,"Quantity"=> pc.count.to_i}
      end
    end
    isp=fis.create_inbound_shipment_plan(ship_from_address, inbound_shipment_plan_request_items, label_prep_preference: "SELLER_LABEL")
    shipments=isp.parse["InboundShipmentPlans"]["member"]
    inbound_shipment=[]
    shipment_event=[]
    shipments.each_with_index do |shipment,i|
      shipment_id=shipment["ShipmentId"]
      shipment_city=shipment["ShipToAddress"]["City"]
      destination_fulfillment_center_id=shipment["DestinationFulfillmentCenterId"]
      inbound_shipment_header= {
        "ShipmentName" => "#{Date.today} - #{shipment_city}",
        "ShipFromAddress" => ship_from_address,
        "DestinationFulfillmentCenterId" => destination_fulfillment_center_id,
        "LabelPrepPreference" => "SELLER_LABEL",
        "ShipmentStatus" => "WORKING"
      }
      inbound_shipment_items=shipment["Items"]["member"]
      inbound_shipment_items.each {|s| s["QuantityShipped"]=s["Quantity"]; s.delete("Quantity")}
      inbound_shipment=fis.create_inbound_shipment(shipment_id, inbound_shipment_header, inbound_shipment_items: inbound_shipment_items)
      @shipment_event[i]=Event.new(event_type: origin, date: Date.today, expected_date: Date.today+30,received_date: Date.today+30,invoice: shipment_id)
      @shipment_event[i].save!
      inbound_shipment_items.each do |item|
        count=shipment_event[i].product_counts.new(sku: Sku.where(name: item["SellerSKU"]).first, count: item["Quantity"])
        count.save!
      end
    end
    
	end
	
	def shipment_details
	  event=Event.find(params[:id])
	end
	
	def label
    require 'barby'
    require 'barby/barcode/code_128'
    require 'barby/outputter/html_outputter'
    event=Event.find(params[:id])
    origin=event.event_type[("Shipping Plan - ".length)..event.event_type.length]
    pc=Sku.products_client(origin)
    @items=Hash.new()
    event.product_counts.sort_by { |a| a.sku.name }.each do |product|
      sku=product.sku
      if product.count>0
        product_info=pc.list_matching_products(kit[1]["ASIN"]).parse["Products"]
        @items[kit[0]]= {
          "Quantity"=> need_28, 
          "FNSKU" => kit[1]["FNSKU"], 
          "barcode" => Barby::Code128A.new(kit[1]["FNSKU"]),
          "Name" => product_info.blank? ? kit[0] : product_info["Product"]["AttributeSets"]["ItemAttributes"]["Title"]}
      end
    end
    render layout: false
  end
	
	def new
		@event=Event.new
		Product.all.count.times do
    	question = @event.product_counts.build
  	end
  end
  
  def load
    date = Date.new(params[:date]["day(1i)"].to_i,params[:date]["day(2i)"].to_i,params[:date]["day(3i)"].to_i)
		ProductCount.import(params[:file],date)
    redirect_to inventory_events_path, notice: "Inventory Loaded."
	end

	def amz_inventory
		@title="Amazon Inventory"
		@origin=params[:origin] || "Amazon US"
	end
  
  def new_inventory2
  	@event=Event.new
  	@event.event_type="Inventory"
  	@title="Inventory Log"
  	@subtitle=""
  	i=0;
  	@products=Array.new;
		Product.all.sort_by(&:name).each do |p|
    	@products[i] = @event.product_counts.build(attributes: { product_id: p.id })
    	i=i+1;
  	end
  	render :new
  end
  
  def new_inventory
  	event=Event.new(date: Date.today, event_type: "Inventory")
  	inv=Product.get_sv_inventory
  	Product.all.sort_by(&:name).each do |p|
    	event.product_counts.build(attributes: { product_id: p.id, count: inv["#{p.id}"], is_box: false })
  	end
  	event.save!
  	redirect_to inventory_events_path
  end
  
  def new_po
  	@event=Event.new(expected_date: Date.today+120,received_date: Date.today+120)
  	@event.event_type="Product Order"
  	@title="New Product Order"
  	@subtitle=""
  	@show_offer = false
   	question = @event.product_counts.build
  end
  
  def load_fba_shipment
  	infile = params[:event][:file].read
  	origin = params[:event][:event_type]
  	date=Date.new(params[:event]["date(1i)"].to_i,params[:event]["date(2i)"].to_i,params[:event]["date(3i)"].to_i)
  	add_cost=params[:event][:additional_cost]
  	Event.load_fba_shipment(infile,origin,date,add_cost)
		flash[:success] = "Shipment Loaded"
		redirect_to fba_events_path
  end
  
  def new_fba
  	@event=Event.new(expected_date: Date.today+30,received_date: Date.today+30)
  	@title="New Inventory Transfer"
  	@subtitle=""
  	@show_offer = true
   	question = @event.product_counts.build
  end

  
  def edit_fba
  	@event=Event.find(params[:id])
  	@show_offer = true
  	@title = "Edit Event"
  end
  
  def receive_po_today
  	@event=Event.find(params[:id])
  	if @event.update_attributes(received_date: Date.today, received: true)
  		flash[:success] = "Purchase Order Received"
  	else
  		flash[:error] = "Purchase Order Was Not Received"
  	end
  	if @event.event_type="Purchase Order"
  		redirect_to po_events_path
  	else
			redirect_to fba_events_path
  	end
  end
  
  def inventory
  	if params[:date_id]
		  @date = Event.find(params[:date_id])
		else
  	  @date = Event.inventory.sort_by(&:date).last
  	end
  	@events=Event.inventory.where(date: @date.date).paginate(:page => params[:page], :per_page => 1)
  	@title= @events.first.event_type
  	render :index
  end
  
  def destroy
  	session[:last_page] = request.env['HTTP_REFERER'] || po_events_url
  	@event=Event.find(params[:id])
  	event_type=@event.event_type
  	@event.destroy
  	redirect_to session[:last_page]
  end
  
  def fba
  	@title="FBA Shipments"
  	@events=Event.where("event_type LIKE ? OR event_type LIKE ?","Amazon Canada", "Amazon US").sort_by(&:created_at).reverse
  	@events=@events.paginate(:page => params[:page], :per_page => 10)
  end
  
  def po
  	@event_type=params[:event_type]
  	if @event_type == "All Shipments"
  		received=""
  	elsif @event_type == "Received"
			received="received = true AND "
  	elsif @event_type == "In Transit"
  		received="received = false AND"
    elsif @event_type == "Late"
  		received="received = false AND expected_date < ? AND"
  	else
  		received=""
  	end
  	@inv_num= params[:Invoice]? params[:Invoice] : ""
  	@sup_name= params[:Supplier]? params[:Supplier] : ""
  	if @event_type == "Late"
    	if @sup_name.length > 0
    		@supplier = Supplier.where("LOWER(name) LIKE ?","%#{@sup_name.downcase}%").first
			  @events=Event.where("#{received} LOWER(invoice) LIKE ? AND supplier_id = ? AND event_type='Product Order'",Date.today,"%#{@inv_num.downcase}%",@supplier.id)
		  else
			  @events=Event.where("#{received} LOWER(invoice) LIKE ? AND event_type='Product Order'",Date.today,"%#{@inv_num.downcase}%")
		  end
		else
		  if @sup_name.length > 0
    		@supplier = Supplier.where("LOWER(name) LIKE ?","%#{@sup_name.downcase}%").first
			  @events=Event.where("#{received} LOWER(invoice) LIKE ? AND supplier_id = ? AND event_type='Product Order'","%#{@inv_num.downcase}%",@supplier.id)
		  else
			  @events=Event.where("#{received} LOWER(invoice) LIKE ? AND event_type='Product Order'","%#{@inv_num.downcase}%")
		  end
		end
		if @inv_num == ""
			@inv_num=[]
		end
		if @sup_name == ""
			@sup_name=[]
		end
  	if params[:sort_by] == "INV_ASC"
  		@events=@events.sort_by {|a| a.invoice }.reverse
  	elsif params[:sort_by] == "INV_DESC"
  		@events=@events.sort_by {|a| a.invoice }
  	elsif params[:sort_by] == "SUP_ASC"
  		@events=@events.sort_by {|a| a.supplier.blank? ? "" : a.supplier.name }.reverse
  	elsif params[:sort_by] == "SUP_DESC"
  		@events=@events.sort_by {|a| a.supplier.blank? ? "" : a.supplier.name }
  	elsif params[:sort_by] == "DC_ASC"
  		@events=@events.sort_by {|a| a.date }.reverse
  	elsif params[:sort_by] == "DC_DESC"
  		@events=@events.sort_by {|a| a.date }
  	elsif params[:sort_by] == "DE_ASC"
  		@events=@events.sort_by {|a| a.expected_date }.reverse
  	elsif params[:sort_by] == "DE_DESC"
  		@events=@events.sort_by {|a| a.expected_date }
  	elsif params[:sort_by] == "DA_ASC"
  		@events=@events.sort_by {|a| a.received_date }.reverse
  	elsif params[:sort_by] == "DA_DESC"
  		@events=@events.sort_by {|a| a.received_date }
  	end
  	@title= "Product Order"
  	@events=@events.paginate(:page => params[:page], :per_page => 10)
  	render :index
  end
  
  def index
    if params[:get_count]
      @po = {quantity: Event.where(event_type: "Product Order").count}.to_xml
    else
      order_count=params[:order_count].to_f
			page=params[:page].to_f
			start=page*order_count+1
			finish=[(page+1)*order_count,Event.where(event_type: "Product Order").count].min
      @po = Event.where(event_type: "Product Order").first(finish).last((finish-start)+1).to_xml(only: 
        [:date, :date,:received_date,:received,:expected_date, :additional_cost, :invoice], 
        include: {supplier: {only: [:name]}, product_counts: {only: [:product_name, :count, :price], include: {product: {only: :sku }}}}, :skip_instruct => false)
    end
    respond_to do |format|
      format.xml { render xml: @po, :layout => false }
    end
  end
  def create
  	@event = Event.new(event_params)
    @event.save
		if @event.event_type == "Inventory"
  	  redirect_to inventory_events_path
  	elsif @event.event_type == "Product Order"
  		@event.product_counts.each do |pc|
  			sp=SupplierPrice.new(date: @event.date, supplier_id: @event.supplier_id, product_id: pc.product_id, quantity: pc.count, price: pc.price/pc.count)
  			sp.save
  		end
  	  redirect_to po_events_path
  	else
  		redirect_to fba_events_path
  	end
  end
  
  def edit
  	@event=Event.find(params[:id])
  	@show_offer = false
  	@title = "Edit Event"
		render :edit_po
  end
  
  def update
  	@event = Event.find(params[:id])
    @event.update_attributes(event_params)
    if @event.event_type == "Inventory"
  	  redirect_to inventory_events_path
  	elsif @event.event_type =="Product Order"
  	  redirect_to po_events_path
  	else
  		redirect_to fba_events_path
  	end
  end
  
  
private


    def event_params
      params.require(:event).permit( :id, :date, :event_type, :invoice, :received_date, :received, :expected_date, :additional_cost, :supplier_id, :product_name, :supplier_name,       product_counts_attributes: [:id, :event_id, :product_id, :count, :is_box, :price, :product_name, '_destroy', :box_count, :piece_count, :sku_name, :offering_id], supplier_attributes: [:id, :supplier_name, :name, '_destroy'] )
    end
end
