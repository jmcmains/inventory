class CreateKeys < ActiveRecord::Migration
  def change
    create_table :keys do |t|
      t.string :name
      t.string :value
      t.string :resource

      t.timestamps
    end
  end
end
