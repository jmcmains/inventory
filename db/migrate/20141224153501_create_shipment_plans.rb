class CreateShipmentPlans < ActiveRecord::Migration
  def change
    create_table :shipment_plans do |t|
      t.integer :plan_id
      t.integer :shipment_id

      t.timestamps null: false
    end
    add_index :shipment_plans, :plan_id
    add_index :shipment_plans, :shipment_id
    add_index :shipment_plans, [:plan_id, :shipment_id], unique: true
  end
end