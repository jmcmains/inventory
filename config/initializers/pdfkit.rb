# config/initializers/pdfkit.rb
PDFKit.configure do |config|
  config.default_options = {
        :disable_smart_shrinking => false,
        :quiet => true,
        :page_size => 'Letter',
        :margin_top => '0.6in',
        :margin_right => '0.001in',
        :margin_bottom => '0.5in',
        :margin_left => '0.2in',
      }

end